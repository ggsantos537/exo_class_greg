<?php
class Education extends Utilisateur {
    private $utilisateurId;
    private $niveau;
    private $domaine;
    private $dateDebut;
    private $dateFin;


    public function __construct($utilisateurId, $niveau, $domaine, $dateDebut, $dateFin) {
        $this->utilisateurId = $utilisateurId;
        $this->niveau = $niveau;
        $this->domaine = $domaine;
        $this->dateDebut = $dateDebut;
        $this->dateFin = $dateFin;
    }

    public function getUtilisateurId() {
        return $this->utilisateurId;
    }

    public function setUtilisateurId($utilisateurId) {
        $this->utilisateurId = $utilisateurId;
    }

    public function getNiveau() {
        return $this->niveau;
    }

    public function setNiveau($niveau) {
        $this->niveau = $niveau;
    }

    public function getDomaine() {
        return $this->domaine;
    }

    public function setDomaine($domaine) {
        $this->domaine = $domaine;
    }

    public function getDateDebut() {
        return $this->dateDebut;
    }

    public function setDateDebut($dateDebut) {
        $this->dateDebut = $dateDebut;
    }

    public function getDateFin() {
        return $this->dateFin;
    }

    public function setDateFin($dateFin) {
        $this->dateFin = $dateFin;
    }
}

?>
