<?php

class Competence extends Utilisateur {
    private $utilisateurId;
    private $nom;

    public function __construct($utilisateurId, $nom) { 
        $this->utilisateurId = $utilisateurId;
        $this->nom = $nom;
    }

    public function getUtilisateurId() {
        return $this->utilisateurId;
    }

    public function setUtilisateurId($utilisateurId) {
        $this->utilisateurId = $utilisateurId;
    }

    public function getNom() {
        return $this->nom;
    }

    public function setNom($nom) {
        $this->nom = $nom;
    }
}

?>