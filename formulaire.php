<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Formulaire</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    
    <h1>FORMULAIRE APERTURE</h1>
    <h2>Veuillez remplir ce formaulaire</h2>

    <form method="POST" action="script.php">
        <label for="nom">NOM :</label><br>
        <input type="text" id="nom" name="nom" required><br><br>

        <label for="prenom">PRENOM :</label><br>
        <input type="text" id="prenom" name="prenom" required><br><br>

        <label for="email">EMAIL :</label><br>
        <input type="email" id="email" name="email" required><br><br>

        <label for="mot_de_passe">MOT DE PASSE :</label><br>
        <input type="password" id="mot_de_passe" name="mot_de_passe" required><br><br>

        <label for="datedenaissance">DATE DE NAISSANCE :</label><br>
        <input type="date" id="date_de_naissance" name="date_de_naissance" required><br><br>

        <label for="genre">GENRE :</label><br>
        <select id="genre" name="genre" required>
            <option value="homme">Homme</option>
            <option value="femme">Femme</option>
            <option value="autre">Autre</option>
        </select><br><br>

        <label for="adresse">ADRESSE :</label><br>
        <input type="text" id="adresse" name="adresse" required><br><br>

        <label for="telephone">TELEPHONE :</label><br>
        <input type="tel" id="telephone" name="telephone" required><br><br>

        <input type="submit" value="ENVOYER"><br><br>
    </form>
    </table>
</body>
</html>
